package com.example.beatstore.service;

import com.example.beatstore.model.Beat;
import com.example.beatstore.repository.BeatRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Getter
@Setter
public class BeatService {


    private final BeatRepository beatRepository;

//    @Autowired
//    EntityManager em;

    @Autowired
    public BeatService(BeatRepository beatRepository) {
        this.beatRepository = beatRepository;
    }

    public List<Beat> getBeats(){
        return beatRepository.findAll();
    }

    public void saveBeat(Beat beat) {
        beatRepository.save(beat);
//        em.merge(beat);
    }


}
