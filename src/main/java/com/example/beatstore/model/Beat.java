package com.example.beatstore.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.mapping.FetchProfile;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "beat", schema = "beat_store")
@Getter
@Setter
public class Beat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private int bpm;

    private String key;

    private String name;

    private String wavPath;

    private String mp3Path;

    private String picturePath;

//
//    @JoinColumn(name = "id", referencedColumnName = "id")
//    @OneToMany(fetch = FetchType.LAZY)
//    private List<Tag> tag;


}
