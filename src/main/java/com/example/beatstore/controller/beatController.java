package com.example.beatstore.controller;

import com.example.beatstore.model.Beat;
import com.example.beatstore.service.BeatService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Getter
@Setter
public class beatController {

    private BeatService beatService;

    @Autowired
    public beatController(BeatService beatService) {
        this.beatService = beatService;
    }

    @GetMapping(value = "/beats")
    List<Beat> getBeats() {
        return beatService.getBeats();
    }

    @PostMapping(value = "/beat")
    void postBeat(@RequestBody Beat newBeat) {
        beatService.saveBeat(newBeat);
    }



}
